"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jwtSecret = exports.saltRounds = void 0;
exports.saltRounds = process.env.SALT_ROUNDS || "10";
exports.jwtSecret = process.env.JWT_SECRET || "goodsecret993";
