"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.bcryptCompareAsync = exports.bcryptHashAsync = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const bcryptHashAsync = (password, saltRounds) => new Promise((resolve, reject) => {
    bcrypt_1.default.hash(password, saltRounds, (err, hash) => {
        if (!err)
            resolve(hash);
        else
            reject(err);
    });
});
exports.bcryptHashAsync = bcryptHashAsync;
const bcryptCompareAsync = (password, hash) => new Promise((resolve, reject) => {
    bcrypt_1.default.compare(password, hash, (err, result) => {
        if (!err)
            resolve(result);
        else
            reject(err);
    });
});
exports.bcryptCompareAsync = bcryptCompareAsync;
