"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const prismaClient_1 = __importDefault(require("../prismaClient"));
const router = express_1.default.Router();
router.patch("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const existingUsername = yield prismaClient_1.default.user.findUnique({
            where: {
                username: req.body.username,
            },
        });
        if (existingUsername) {
            // username exist
            return res.status(400).json({ error: "Username already exists" });
        }
        const user = yield prismaClient_1.default.user.update({
            where: { id: req.auth.id },
            data: {
                username: req.body.username,
            },
        });
        res.json();
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: "Internal server error" });
    }
}));
router.delete("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield prismaClient_1.default.user.delete({
            where: { id: req.auth.id },
        });
        res.json();
    }
    catch (error) {
        console.log(error);
        return res.status(500);
    }
}));
exports.default = router;
