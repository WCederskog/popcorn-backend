"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const prismaClient_1 = __importDefault(require("../prismaClient"));
const router = express_1.default.Router();
router.post("/toggle-favorite", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let media;
    try {
        media = yield prismaClient_1.default.media.findFirst({
            where: {
                OR: [{ name: req.body.media.name }, { title: req.body.media.title }],
            },
        });
    }
    catch (error) {
        console.error("Error checking existing media:", error);
        return res.status(500).json();
    }
    try {
        //favorite stuff here
        if (media) {
            console.log("Media already exists:");
        }
        else {
            console.log("Creating new media:");
            media = yield prismaClient_1.default.media.create({
                data: {
                    mediaId: req.body.media.id.toString(),
                    name: req.body.media.name,
                    title: req.body.media.title,
                    posterPath: req.body.media.poster_path,
                    releaseDate: req.body.media.release_date,
                    firstAirDate: req.body.media.first_air_date,
                },
            });
            console.log("New media created:", media);
        }
        const favorite = yield prismaClient_1.default.favoritesOnUsers.findFirst({
            where: {
                AND: [{ mediaId: media.id }, { userId: req.auth.id }],
            },
        });
        if (favorite) {
            yield prismaClient_1.default.favoritesOnUsers.delete({
                where: {
                    mediaId_userId: { mediaId: media.id, userId: req.auth.id },
                },
            });
        }
        else {
            const newFavorite = yield prismaClient_1.default.favoritesOnUsers.create({
                data: {
                    userId: req.auth.id,
                    mediaId: media.id,
                },
            });
        }
        console.log(favorite);
        // Send a success response to the client
        return res.status(200).json();
    }
    catch (error) {
        console.error("Error adding to favorites:", error);
        return res.status(500).json({ success: false, error: "Server Error" });
    }
}));
router.get("/favorites", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Get the user favorite movies
        const favorites = yield prismaClient_1.default.favoritesOnUsers.findMany({
            where: {
                userId: req.auth.id,
            },
            include: { Media: true },
        });
        return res.status(200).json({ success: true, favorites });
    }
    catch (error) {
        console.error("Error fetching favorite movies:", error);
        return res.status(500).json({ success: false, error: "Server Error" });
    }
}));
router.post("/toggle-watchlist", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let media;
    try {
        media = yield prismaClient_1.default.media.findFirst({
            where: {
                OR: [{ name: req.body.media.name }, { title: req.body.media.title }],
            },
        });
    }
    catch (error) {
        console.error("Error checking existing media:", error);
        return res.status(500).json();
    }
    try {
        //favorite stuff here
        if (media) {
            console.log("Media already exists:");
        }
        else {
            console.log("Creating new media:");
            media = yield prismaClient_1.default.media.create({
                data: {
                    mediaId: req.body.media.id.toString(),
                    name: req.body.media.name,
                    title: req.body.media.title,
                    posterPath: req.body.media.poster_path,
                    releaseDate: req.body.media.release_date,
                    firstAirDate: req.body.media.first_air_date,
                },
            });
            console.log("New media created:", media);
        }
        const watchlist = yield prismaClient_1.default.watchlistOnUsers.findFirst({
            where: {
                AND: [{ mediaId: media.id }, { userId: req.auth.id }],
            },
        });
        if (watchlist) {
            yield prismaClient_1.default.watchlistOnUsers.delete({
                where: {
                    mediaId_userId: { mediaId: media.id, userId: req.auth.id },
                },
            });
        }
        else {
            const newWatchlist = yield prismaClient_1.default.watchlistOnUsers.create({
                data: {
                    userId: req.auth.id,
                    mediaId: media.id,
                },
            });
        }
        console.log(watchlist);
        // Send a success response to the client
        return res.status(200).json();
    }
    catch (error) {
        console.error("Error adding to favorites:", error);
        return res.status(500).json({ success: false, error: "Server Error" });
    }
}));
router.get("/watchlist", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Get the user favorite movies
        const watchlists = yield prismaClient_1.default.watchlistOnUsers.findMany({
            where: {
                userId: req.auth.id,
            },
            include: { Media: true },
        });
        return res.status(200).json({ success: true, watchlists });
    }
    catch (error) {
        console.error("Error fetching favorite movies:", error);
        return res.status(500).json({ success: false, error: "Server Error" });
    }
}));
exports.default = router;
