"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const prismaClient_1 = __importDefault(require("../prismaClient"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const constants_1 = require("../constants");
const brypctUtils_1 = require("../bcrypt/brypctUtils");
const router = express_1.default.Router();
router.post("/signup", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (req.body.password !== req.body.confirmPassword) {
            // Email already exists
            return res.status(400).json({ error: "Passwords do not match" });
        }
        const existingEmail = yield prismaClient_1.default.user.findUnique({
            where: {
                email: req.body.email,
            },
        });
        if (existingEmail) {
            // email already exists
            return res.status(400).json({ error: "Email already exists" });
        }
        const existingUsername = yield prismaClient_1.default.user.findUnique({
            where: {
                username: req.body.username,
            },
        });
        if (existingUsername) {
            // username already exists
            return res.status(400).json({ error: "Username already exists" });
        }
        // Hash the password
        const hashedPassword = yield (0, brypctUtils_1.bcryptHashAsync)(req.body.password, parseInt(constants_1.saltRounds));
        const user = yield prismaClient_1.default.user.create({
            data: {
                email: req.body.email,
                password: hashedPassword,
                username: req.body.username,
            },
        });
        const users = yield prismaClient_1.default.user.findMany();
        console.log(users);
        res.json(user);
    }
    catch (error) {
        console.error("Error creating user:", error);
        res.status(500).json({ error: "Internal server error" });
    }
}));
router.post("/login", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const existingUser = yield prismaClient_1.default.user.findUnique({
            where: {
                email: req.body.email,
            },
        });
        if (!existingUser) {
            // Email doesn't exist
            return res.status(400).json({ error: "Wrong credentials" });
        }
        // Compare password
        const passwordMatches = yield (0, brypctUtils_1.bcryptCompareAsync)(req.body.password, existingUser.password);
        if (!passwordMatches) {
            return res.status(401).json({ error: "Wrong credentials" });
        }
        const tokenData = {
            email: existingUser.email,
            id: existingUser.id,
        };
        const accessToken = jsonwebtoken_1.default.sign(tokenData, constants_1.jwtSecret, {
            expiresIn: "7d",
            algorithm: "HS256",
        });
        console.log("logged in");
        res.json({ token: accessToken, username: existingUser.username });
    }
    catch (error) {
        console.log(error);
        return res.status(500);
    }
}));
exports.default = router;
