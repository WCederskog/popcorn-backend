"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const express_1 = __importDefault(require("express"));
const authentication_1 = __importDefault(require("./routes/authentication"));
const cors_1 = __importDefault(require("cors"));
const comment_1 = __importDefault(require("./routes/comment"));
const media_1 = __importDefault(require("./routes/media"));
const express_jwt_1 = require("express-jwt");
const constants_1 = require("./constants");
const user_1 = __importDefault(require("./routes/user"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const app = (0, express_1.default)();
const authMiddleware = (0, express_jwt_1.expressjwt)({
    secret: constants_1.jwtSecret,
    algorithms: ["HS256"],
});
// Middleware
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use(authMiddleware.unless({ path: ["/auth/login", "/auth/signup"] }));
// Routes
app.use("/auth", authentication_1.default);
app.use("/comment", comment_1.default);
app.use("/media", media_1.default);
app.use("/user", user_1.default);
// Swagger
app.use("/swagger", swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(undefined, { swaggerOptions: { url: "/swagger.json" } }));
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
