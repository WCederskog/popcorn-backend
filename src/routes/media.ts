import express from "express";
import prisma from "../prismaClient";
const router = express.Router();

router.post("/toggle-favorite", async (req, res) => {
  let media;
  try {
    media = await prisma.media.findFirst({
      where: {
        OR: [{ name: req.body.media.name }, { title: req.body.media.title }],
      },
    });
  } catch (error) {
    console.error("Error checking existing media:", error);
    return res.status(500).json();
  }

  try {
    if (media) {
      console.log("Media already exists:");
    } else {
      console.log("Creating new media:");
      media = await prisma.media.create({
        data: {
          mediaId: req.body.media.id.toString(),
          name: req.body.media.name,
          title: req.body.media.title,
          posterPath: req.body.media.poster_path,
          releaseDate: req.body.media.release_date,
          firstAirDate: req.body.media.first_air_date,
        },
      });
      console.log("New media created:", media);
    }

    const favorite = await prisma.favoritesOnUsers.findFirst({
      where: {
        AND: [{ mediaId: media.id }, { userId: req.auth.id }],
      },
    });
    if (favorite) {
      await prisma.favoritesOnUsers.delete({
        where: {
          mediaId_userId: { mediaId: media.id, userId: req.auth.id },
        },
      });
    } else {
      const newFavorite = await prisma.favoritesOnUsers.create({
        data: {
          userId: req.auth.id,
          mediaId: media.id,
        },
      });
    }
    return res.status(200).json();
  } catch (error) {
    console.error("Error adding to favorites:", error);
    return res.status(500).json({ success: false, error: "Server Error" });
  }
});

router.get("/favorites", async (req, res) => {
  try {
    const favorites = await prisma.favoritesOnUsers.findMany({
      where: {
        userId: req.auth.id,
      },
      include: { Media: true },
    });

    return res.status(200).json({ success: true, favorites });
  } catch (error) {
    console.error("Error fetching favorite movies:", error);
    return res.status(500).json({ success: false, error: "Server Error" });
  }
});

router.post("/toggle-watchlist", async (req, res) => {
  let media;
  try {
    media = await prisma.media.findFirst({
      where: {
        OR: [{ name: req.body.media.name }, { title: req.body.media.title }],
      },
    });
  } catch (error) {
    console.error("Error checking existing media:", error);
    return res.status(500).json();
  }

  try {
    if (media) {
      console.log("Media already exists:");
    } else {
      console.log("Creating new media:");
      media = await prisma.media.create({
        data: {
          mediaId: req.body.media.id.toString(),
          name: req.body.media.name,
          title: req.body.media.title,
          posterPath: req.body.media.poster_path,
          releaseDate: req.body.media.release_date,
          firstAirDate: req.body.media.first_air_date,
        },
      });
      console.log("New media created:", media);
    }

    const watchlist = await prisma.watchlistOnUsers.findFirst({
      where: {
        AND: [{ mediaId: media.id }, { userId: req.auth.id }],
      },
    });
    if (watchlist) {
      await prisma.watchlistOnUsers.delete({
        where: {
          mediaId_userId: { mediaId: media.id, userId: req.auth.id },
        },
      });
    } else {
      const newWatchlist = await prisma.watchlistOnUsers.create({
        data: {
          userId: req.auth.id,
          mediaId: media.id,
        },
      });
    }
    return res.status(200).json();
  } catch (error) {
    console.error("Error adding to favorites:", error);
    return res.status(500).json({ success: false, error: "Server Error" });
  }
});

router.get("/watchlist", async (req, res) => {
  try {
    const watchlists = await prisma.watchlistOnUsers.findMany({
      where: {
        userId: req.auth.id,
      },
      include: { Media: true },
    });

    return res.status(200).json({ success: true, watchlists });
  } catch (error) {
    console.error("Error fetching favorite movies:", error);
    return res.status(500).json({ success: false, error: "Server Error" });
  }
});

export default router;
