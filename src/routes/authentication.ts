import express from "express";
import prisma from "../prismaClient";
import jsonwebtoken from "jsonwebtoken";
import { jwtSecret, saltRounds } from "../constants";
import { bcryptCompareAsync, bcryptHashAsync } from "../bcrypt/brypctUtils";
const router = express.Router();

router.post("/signup", async (req, res) => {
  try {
    if (
      !req.body.email
        .toLowerCase()
        .match(
          /[a-z0-9!#$%&'+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'+/=?^_`{|}~-]+)@(?:[a-z0-9](?:[a-z0-9-][a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
        )
    ) {
      return res.status(400).json({ error: "Not a valid email" });
    }

    const existingEmail = await prisma.user.findUnique({
      where: {
        email: req.body.email,
      },
    });

    if (existingEmail) {
      // email already exists
      return res.status(400).json({ error: "Email already exists" });
    }

    const existingUsername = await prisma.user.findUnique({
      where: {
        username: req.body.username,
      },
    });

    if (existingUsername) {
      // username already exists
      return res.status(400).json({ error: "Username already exists" });
    }

    // Hash the password
    const hashedPassword = await bcryptHashAsync(
      req.body.password,
      parseInt(saltRounds)
    );

    const user = await prisma.user.create({
      data: {
        email: req.body.email,
        password: hashedPassword,
        username: req.body.username,
      },
    });

    const users = await prisma.user.findMany();
    console.log(users);
    res.json(user);
  } catch (error) {
    console.error("Error creating user:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/login", async (req, res) => {
  try {
    const existingUser = await prisma.user.findUnique({
      where: {
        email: req.body.email,
      },
    });
    if (!existingUser) {
      // Email doesn't exist
      return res.status(400).json({ error: "Wrong credentials" });
    }
    // Compare password
    const passwordMatches = await bcryptCompareAsync(
      req.body.password,
      existingUser.password
    );
    if (!passwordMatches) {
      return res.status(401).json({ error: "Wrong credentials" });
    }

    const tokenData = {
      email: existingUser.email,
      id: existingUser.id,
    };

    const accessToken = jsonwebtoken.sign(tokenData, jwtSecret, {
      expiresIn: "7d",
      algorithm: "HS256",
    });

    console.log("logged in");
    res.json({ token: accessToken, username: existingUser.username });
  } catch (error) {
    console.log(error);
    return res.status(500);
  }
});

export default router;
