import express from "express";
import prisma from "../prismaClient";

const router = express.Router();

router.patch("/", async (req, res) => {
  try {
    const existingUsername = await prisma.user.findUnique({
      where: {
        username: req.body.username,
      },
    });

    if (existingUsername) {
      return res.status(400).json({ error: "Username already exists" });
    }

    const user = await prisma.user.update({
      where: { id: req.auth.id },
      data: {
        username: req.body.username,
      },
    });

    res.json();
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/", async (req, res) => {
  try {
    const user = await prisma.user.delete({
      where: { id: req.auth.id },
    });

    res.json();
  } catch (error) {
    console.log(error);
    return res.status(500);
  }
});

export default router;
