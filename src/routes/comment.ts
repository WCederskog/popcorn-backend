import express from "express";
import prisma from "../prismaClient";

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const comment = await prisma.comment.create({
      data: {
        text: req.body.text,
        authorId: req.body.userId,
        mediaId: req.body.mediaId,
      },
    });

    res.json();
  } catch (error) {
    console.log(error);
    return res.status(500);
  }
});

export default router;
