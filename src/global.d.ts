declare namespace Express {
  export interface User {
    id: number;
    email: string;
  }
  interface Request {
    auth: User;
  }
}
