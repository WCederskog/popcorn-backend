import "dotenv/config";
import express from "express";
import authentication from "./routes/authentication";
import cors from "cors";
import comment from "./routes/comment";
import media from "./routes/media";
import { expressjwt } from "express-jwt";
import { jwtSecret } from "./constants";
import user from "./routes/user";

const app = express();

const authMiddleware = expressjwt({
  secret: jwtSecret,
  algorithms: ["HS256"],
});

// Middleware
app.use(cors());
app.use(express.json());

app.use(authMiddleware.unless({ path: ["/auth/login", "/auth/signup"] }));

// Routes
app.use("/auth", authentication);
app.use("/comment", comment);
app.use("/media", media);
app.use("/user", user);

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
