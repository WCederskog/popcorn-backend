/*
  Warnings:

  - Added the required column `mediaType` to the `Media` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Media" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "mediaId" TEXT NOT NULL,
    "mediaType" TEXT NOT NULL,
    "name" TEXT,
    "title" TEXT,
    "posterPath" TEXT,
    "releaseDate" TEXT,
    "firstAirDate" TEXT
);
INSERT INTO "new_Media" ("firstAirDate", "id", "mediaId", "name", "posterPath", "releaseDate", "title") SELECT "firstAirDate", "id", "mediaId", "name", "posterPath", "releaseDate", "title" FROM "Media";
DROP TABLE "Media";
ALTER TABLE "new_Media" RENAME TO "Media";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
