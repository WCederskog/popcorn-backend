-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_GenresOnMedia" (
    "mediaId" INTEGER NOT NULL,
    "genreId" INTEGER NOT NULL,

    PRIMARY KEY ("mediaId", "genreId"),
    CONSTRAINT "GenresOnMedia_mediaId_fkey" FOREIGN KEY ("mediaId") REFERENCES "Media" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "GenresOnMedia_genreId_fkey" FOREIGN KEY ("genreId") REFERENCES "Genre" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_GenresOnMedia" ("genreId", "mediaId") SELECT "genreId", "mediaId" FROM "GenresOnMedia";
DROP TABLE "GenresOnMedia";
ALTER TABLE "new_GenresOnMedia" RENAME TO "GenresOnMedia";
CREATE TABLE "new_Comment" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "text" TEXT NOT NULL,
    "authorId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "mediaId" INTEGER NOT NULL,
    CONSTRAINT "Comment_authorId_fkey" FOREIGN KEY ("authorId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "Comment_mediaId_fkey" FOREIGN KEY ("mediaId") REFERENCES "Media" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_Comment" ("authorId", "createdAt", "id", "mediaId", "text") SELECT "authorId", "createdAt", "id", "mediaId", "text" FROM "Comment";
DROP TABLE "Comment";
ALTER TABLE "new_Comment" RENAME TO "Comment";
CREATE TABLE "new_WatchlistOnUsers" (
    "userId" INTEGER NOT NULL,
    "mediaId" INTEGER NOT NULL,

    PRIMARY KEY ("mediaId", "userId"),
    CONSTRAINT "WatchlistOnUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "WatchlistOnUsers_mediaId_fkey" FOREIGN KEY ("mediaId") REFERENCES "Media" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_WatchlistOnUsers" ("mediaId", "userId") SELECT "mediaId", "userId" FROM "WatchlistOnUsers";
DROP TABLE "WatchlistOnUsers";
ALTER TABLE "new_WatchlistOnUsers" RENAME TO "WatchlistOnUsers";
CREATE TABLE "new_FavoritesOnUsers" (
    "userId" INTEGER NOT NULL,
    "mediaId" INTEGER NOT NULL,

    PRIMARY KEY ("mediaId", "userId"),
    CONSTRAINT "FavoritesOnUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "FavoritesOnUsers_mediaId_fkey" FOREIGN KEY ("mediaId") REFERENCES "Media" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_FavoritesOnUsers" ("mediaId", "userId") SELECT "mediaId", "userId" FROM "FavoritesOnUsers";
DROP TABLE "FavoritesOnUsers";
ALTER TABLE "new_FavoritesOnUsers" RENAME TO "FavoritesOnUsers";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
