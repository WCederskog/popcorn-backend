/*
  Warnings:

  - You are about to alter the column `mediaId` on the `Media` table. The data in that column could be lost. The data in that column will be cast from `String` to `Int`.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Media" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "mediaId" INTEGER NOT NULL,
    "name" TEXT,
    "title" TEXT,
    "posterPath" TEXT,
    "releaseDate" TEXT,
    "firstAirDate" TEXT
);
INSERT INTO "new_Media" ("firstAirDate", "id", "mediaId", "name", "posterPath", "releaseDate", "title") SELECT "firstAirDate", "id", "mediaId", "name", "posterPath", "releaseDate", "title" FROM "Media";
DROP TABLE "Media";
ALTER TABLE "new_Media" RENAME TO "Media";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
